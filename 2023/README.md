<!-- Entries between SOLUTIONS and RESULTS tags are auto-generated -->

[![AoC](https://badgen.net/badge/AoC/2023/blue)](https://adventofcode.com/2023)

# 🎄 Advent of Code 2023 🎄

## Installation

```
npm i
```

## Running in dev mode

```
npm start <day>
```

Example:

```
npm start 1
```

---

✨🎄🎁🎄🎅🎄🎁🎄✨
